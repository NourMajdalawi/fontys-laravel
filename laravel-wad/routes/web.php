<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@getIndex');
Route::get('/announcement','AnnouncementController@getIndex');
Route::get('/announcement/add','AnnouncementController@getAddAnnouncement');
Route::post('/announcement/add','AnnouncementController@postAddAnnouncement');
Route::get('/announcement/{id}','AnnouncementController@getAnnouncement');
Route::post('/avatars', function() { request()->file('avatar')->store('public/avatars'); return back();});
Route::get('/announcement/{id}/delete','AnnouncementController@deleteAnnouncement');
Route::get('/announcement/{id}/edit','AnnouncementController@editAnnouncement');
Route::post('/announcement/{id}/update','AnnouncementController@updateAnnouncement');

Route::get('/upload','IndexController@getUpload');

Route::post('/home','HomeController@showUploadFile');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

