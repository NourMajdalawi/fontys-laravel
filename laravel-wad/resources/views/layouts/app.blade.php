<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ url('styles/styles.css') }}">

</head>

<body>
<header id="page_header">
    <a href="{{action('IndexController@getIndex')}}" id="home_link"><img src="{{url('images/logo.png')}}" alt="Link to home"></a>
</header>
<nav id="page_navigation">
    <ol>
        <li><a href="{{action('IndexController@getIndex')}}">Home</a></li>
        <li><a href="{{action('AnnouncementController@getIndex')}}">Announcement</a></li>
        <li><a href="{{action('AnnouncementController@getAddAnnouncement')}}">Add Announcement</a></li>
        @guest
            <li>
                <a href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li>
                    <a href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="dropdown">
                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <a class="dropdown-item" href="{{action('IndexController@getUpload')}}">Upload image</a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ol>
</nav>
<main id="page_content">
    @yield('content')
</main>

<footer id="main-footer">
    <div id="footer-bottom">
        <div class="container clearfix">
            <ul class="et-social-icons">
                <li class="et-social-icon et-social-facebook">
                    <a href="https://www.facebook.com/FontysHogescholen/" class="icon">
                        <span>Facebook</span>
                    </a></li><li class="et-social-icon et-social-twitter">
                    <a href="https://twitter.com/fontys" class="icon">
                        <span>Twitter</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-google-plus">
                    <a href="https://plus.google.com/115177765459489012856/posts" class="icon">
                        <span>Google</span>
                    </a></li>
                <li class="et-social-icon et-social-rss">
                    <a href="https://fontysblogt.nl/feed/" class="icon">
                        <span>RSS</span>
                    </a>
                </li>
            </ul><p id="footer-info">
                <a href="https://www.fontys.nl/">Fontys Hogescholen</a>
            </p>
        </div>
    </div>
</footer>

</body>
</html>
