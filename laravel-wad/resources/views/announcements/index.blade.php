@extends('layouts.app')

@section('title', 'Announcements')

@section('content')
    <h1>Announcements</h1>
    @foreach ($announcements as $a)
        <article>
            <h2>{{$a->title}}</h2>
            <i>Written by {{ $a->user->name }}</i>
            <p>
                {{Str::limit($a->message)}}
            </p>
            <footer>
                <a href="{{action('AnnouncementController@getAnnouncement', ['id'=>$a->id])}}">Read more...</a>

            </footer>
        </article>
    @endforeach
@endsection

@section('footer')

@endsection
