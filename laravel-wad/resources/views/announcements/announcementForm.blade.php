@extends('layouts.app')

@section('title', 'Add New Announcement')

@section('content')
    <h1 class="text">Add new announcement</h1>

    <form method="POST" class="announcementForm">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @csrf
        <div class="grid-adding">
            <label class="item1">Title:*</label>
            <input class="item2" type="text" name="title" value="{{old('title')}}">
            <label class="item3">Message:*</label>
            <input class="item4" name="message">{{old('message')}}

            <p>(*) Required</p>
        </div>
        <br>
        <button type="submit" class="button">Add the announcement</button>
    </form>
@endsection
