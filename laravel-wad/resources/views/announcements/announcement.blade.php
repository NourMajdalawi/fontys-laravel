@extends('layouts.app')

@section('title', 'Announcement overview')

@section('content')
    <h1>{{$announcement->title}}</h1>
    <i>Written by {{ $announcement->user->name }}</i>
    <p>
        {{$announcement->message}}
    </p>

    <footer>
        <a href="{{action('AnnouncementController@deleteAnnouncement', ['id'=>$announcement->id])}}" type="submit" class="buttoncrud">Delete the announcement</a>
        <a href="{{action('AnnouncementController@editAnnouncement', ['id'=>$announcement->id])}}" type="submit" class="buttoncrud">Edit the announcement</a>

    </footer>
@endsection

@section('footer')

@endsection
