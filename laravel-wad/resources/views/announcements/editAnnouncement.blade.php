@extends('layouts.app')

@section('announcement')

@section('content')
    <h1 class="text">Edit the announcement</h1>

    <form method="POST" class="announcementForm">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @csrf
        <div class="grid-adding">
            <label class="item1">Title:*</label>
            <input class="item2" type="text" name="title" value={{$announcement->title}}>
            <label class="item3">Message:*</label>
            <input class="item4" name="message" value={{$announcement->message}}>

            <p>(*) Required</p>
        </div>
        <br>
        <button type="submit" class="button">Edit the announcement</button>
    </form>
@endsection
