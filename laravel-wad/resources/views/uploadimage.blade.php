@extends('layouts.app')

@section('title', 'Welcome')

@section('content')
    {{--    <h1>hi {{ Auth::user()->name }}</h1>--}}
    <h1>Welcome, {{ Auth::user()->name }}</h1>
    <h2>You can upload your photo here !</h2>
    <form enctype="multipart/form-data" method = POST action = "/avatars">
        @csrf<input type = "file" name = "avatar" id = "avatar">
        <button type="submit">Upload</button>
    </form>

@endsection

@section('footer')
    this is a footer for welcome view
@endsection
