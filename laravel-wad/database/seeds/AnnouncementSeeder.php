<?php

use App\Announcement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i =1 ;$i<=11; $i++){
            $obj = new Announcement();
            $obj->title = Str::random(10);
            $obj->message = Str::random(100);

            $obj->save();
        }
    }
}
