<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //starting page for the website
    //action->ulr to specific function
    public  function getIndex(){
        return view('welcome',['username'=>'Nour and Simone']);
    }

    public function getUpload(){
        return view('uploadimage');
    }
}
