<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AnnouncementController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    public function getIndex()
    {
        // Retrieve all the annoucements
        $announcement = Announcement::with('user')->orderBy('id', 'desc')->get();
        // Generate 10 random announcements

        // Show it in a the view
        return view('announcements.index', ['announcements' => $announcement] );
    }

    public  function getAnnouncement($id){
        $announcement = Announcement::FindOrFail($id);
        return view('announcements.announcement', ['announcement'=>$announcement]);
    }
    public function getAddAnnouncement() {
       $this->authorize('create', Announcement::class);

        return view('announcements.announcementForm');
    }
    public  function postAddAnnouncement(Request $request){
       $this->authorize('create', Announcement::class);
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('AnnouncementController@getAddAnnouncement')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $newAnnouncement = new Announcement();
            $newAnnouncement->title = $request->input('title');
            $newAnnouncement->message = $request->input( 'message' );
            $newAnnouncement->save();
            Auth::user()->announcements()->save($newAnnouncement);

            return redirect()->action( 'AnnouncementController@getIndex');
        }

    }
    public function deleteAnnouncement($id){
        DB::table('announcements')->where('id', '=', $id)->delete();
        $announcement = Announcement::with('user')->orderBy('id', 'desc')->get();
        return view('announcements.index', ['announcements' => $announcement] );
    }

    public function editAnnouncement($id){
        $announcement = Announcement::findOrFail($id);
        return view('announcements.editAnnouncement', ['announcement' => $announcement]);
    }

    public function updateAnnouncement(Request $request, $id){


        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('AnnouncementController@getAddAnnouncement')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $newAnnouncement = Announcement::find($id);
            $newAnnouncement->title = $request->input('title');
            $newAnnouncement->message = $request->input( 'message' );
            $newAnnouncement->save();

            return view('announcements.announcement', ['announcement'=>$newAnnouncement]);
        }
    }
}
